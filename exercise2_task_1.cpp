// Exercise_2/src/Task_1.cpp
#include <iostream>     // cout
#include <iomanip>      // setw()
using namespace std;

int main()
{
    // Deklara variabler a(katet), b(katet), c(hypotenusa), counter räknar ut
    // hur många kombinationer finns så att c ^ 2 = a ^ 2 + b ^ 2
   int a, b, c, counter = 0;

   // Tre for-loop används för att räkna ut antal kombinationsmöjligheter
   for(int c = 1; c <= 500; c++)
   {
      for(int b = 1; b <= 500; b++)
      {
          // a = b gör att man inte räknar samma kommbination två gånger
          // t. ex både a = 3, b = 4 och a = 4, b = 3 ger c = 5

         for(int a = b; a <= 500; a++)
            {
               if(c*c == a*a + b*b)
                 {
                    counter ++;  // Om c^2 = a^2 + b^2, kombinationsmöjligheter +1

                    // Om c = n * 100 (alltså 100 - 500), skriv ut värdet av a, b, c
                    if(c % 100 == 0)
                    {
                       cout << "  Katet a      :" << setw(5) << a << setw(5);
                       cout << "  katet b      :" << setw(5) << b << setw(5);
                       cout << "  hypotenusa c :" << setw(5) << c << endl;
                    }
                 }
            }
      }
   }

   // Skriv resultatet
   cout << endl << "  The amount of Pythagorean triangles are: " << counter << endl;


   return 0;
}
