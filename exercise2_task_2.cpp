// Exercise_2/src/Task_1.cpp

#include <iostream>   // cout
#include <iomanip>    // setw()
using namespace std;

int main()
{
    // Deklarera variabel ascii nummer, man börjar med 32
    int ascii_num = 32;

    // Skriv ut ascii tecken (mellan 32 - 255)
    for(int row = 1; row <= 32; row ++)          // 32 rader
    {
        for(int kol = 1; kol <= 7; kol ++)       // 7 element per rad
        {
            // Skriv ut ett ascii tecken
            cout << setw(5)<< ascii_num << setw(5) << static_cast<char>(ascii_num) << setw(5);

            ascii_num++;                        // Värdet på ascii nummer ökas med 1
        }
        cout << endl;                           // Byt rad
    }
    return 0;

}
